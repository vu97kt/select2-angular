import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dataList = [
    {name: 'tada 1ssssssssssssssssssssssssssssss', id: 1},
    {name: 'tada 2', id: 2},
    {name: 'tada 3', id: 3},
    {name: 'tada 4', id: 4},
    {name: 'tada 5', id: 5},
    {name: 'tada 6', id: 6},
    {name: 'tada 7', id: 7},
    {name: 'tada 8', id: 8},
    {name: 'tada 9', id: 9},
  ];

  configSelect = {
    placeholderSelect: 'Chọn người dùng',
    placeholderSelect2: 'Tìm kiếm'
  }

  configSelect2 ={
    placeholderSelect: 'Chọn người dùng',
    placeholderSelect2: 'Tìm kiếm',
    isMultiChecked: true
  }

  onChangeSelect(value: any){
    console.log(value)
  }
}
