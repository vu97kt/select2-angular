import {Component, OnInit, ViewChild, ElementRef, Renderer2, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

interface ConfigOptionInterface {
  isReadOnly?: boolean, /** Chỉ đọc **/
  isMultiChecked?: boolean, /** neu ma false thì chỉ một lựa chọn còn true thì được chọn nhiều lựa chọn **/
  hiddenSearchSelect2?: boolean,  /** Ẩn search của select2 **/
  placeholderSelect2?: string, /** placeholder search select 2 **/
  placeholderSelect?: string
}

@Component({
  selector: 'app-ngx-select2',
  templateUrl: './ngx-select2.component.html',
  styleUrls: ['./ngx-select2.component.scss']
})

export class NgxSelect2Component implements OnInit, OnChanges{
  @Input() nameDisplay: string | any; /** Gía trị hiển thị **/
  @Input() configOption: ConfigOptionInterface | any; /** Cấu hình select **/
  @Input() dataSelect: any[] | any;
  @Input() value: string | any;
  @ViewChild('toggleButton') toggleButton: ElementRef | any;
  @ViewChild('toggleSelect2') toggleSelect2: ElementRef | any;
  @Output('onChangeSelect') dataOutput = new EventEmitter<any>();

  isToggle = false;
  resultMultiChoose: any[] = [];
  resultSingleChoose: any;
  selectDisplay: any[] = [];
  isCheckAll = false;

  constructor(private renderer: Renderer2) {
    this.renderer.listen('window', 'click',(e:Event)=>{
      console.log(this.toggleSelect2, e.target);
      if(this.toggleSelect2?.nativeElement && e.target !== this.toggleButton.nativeElement && e.composedPath().indexOf(this.toggleSelect2.nativeElement) == -1){
        this.isToggle=false;
        console.log(13)
        if (this.resultMultiChoose.length > 0) this.dataOutput.emit(this.resultMultiChoose.map((item: any) => item[this.value]));
      }
    });
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (this.configOption.isMultiChecked) this.dataSelect.forEach((item: any) => item.checked = false);
    this.selectDisplay = this.dataSelect;
  }

  selectAll(): any{
    this.isCheckAll = !this.isCheckAll;
    if (this.isCheckAll){
      this.dataSelect.forEach((item: any) => item.checked = true);
      this.selectDisplay.forEach((item: any) => item.checked = true);
      return this.resultMultiChoose = this.selectDisplay;
    }

    this.dataSelect.forEach((item: any) => item.checked = false);
    this.selectDisplay.forEach((item: any) => item.checked = false);
    this.resultMultiChoose = [];
  }

  searchSelect2(event: any){
    let keyword = event.target.value, dataSelect = this.dataSelect;
    if (keyword == null || keyword == undefined || keyword == '') return this.selectDisplay = this.dataSelect;
    let dataResult = dataSelect.filter((item: any) => item[this.nameDisplay].indexOf(keyword) >-1);
    this.selectDisplay = dataResult;
  }

  toggleSelect() {
    this.isToggle = !this.isToggle
    if (this.isToggle) this.selectDisplay = this.dataSelect;
  }

  chooseSingleOption(item: any): any{
    if (this.resultSingleChoose != item){
      this.resultSingleChoose = item;
      this.isToggle = false;
      this.dataOutput.emit(item ? item[this.value]: item);
    }
  }

  chooseMultiOption(event: any, option: any): any{
    let isChecked = event.checked;
    if (isChecked){
      this.changeCheckedData(option, true);
      this.isCheckAll = this.selectDisplay.filter((item: any) => item.checked == true).length == this.selectDisplay.length ?? false;
    }else {
      this.isCheckAll = false;
      this.changeCheckedData(option);
    }
  }

  changeCheckedData(option: any, isTrue?:boolean){
    let findIndexDataSelect = this.dataSelect.findIndex((item: any) => item[this.value] == option[this.value]);
    let findIndexDataSelectDisplay = this.selectDisplay.findIndex((item: any) => item[this.value] == option[this.value]);
    this.dataSelect[findIndexDataSelect].checked = isTrue ?? false;
    this.selectDisplay[findIndexDataSelectDisplay].checked = isTrue ?? false;
    this.resultMultiChoose = this.selectDisplay.filter((item: any) => item.checked === true);
  }

  removeOption(option: any){
    this.changeCheckedData(option);
    this.dataOutput.emit(this.resultMultiChoose.map((item: any) => item[this.value]));
    this.isCheckAll = false;
  }
}
