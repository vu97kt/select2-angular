import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxSelect2Component } from './ngx-select2.component';

describe('NgxSelect2Component', () => {
  let component: NgxSelect2Component;
  let fixture: ComponentFixture<NgxSelect2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxSelect2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxSelect2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
